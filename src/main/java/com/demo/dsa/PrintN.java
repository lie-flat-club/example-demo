package com.demo.dsa;

public class PrintN {

  public static void main(String[] args) {
    printN(100000);
    System.out.println("====");
    printN2(100000);
  }

  public static void printN(int n) {
    for (int i = 0; i < n; i++) {
      System.out.println(i);
    }
  }

  public static void printN2(int n) {
    if (n > 0) {
      printN2(n-1);
      System.out.println(n);
    }
  }
}
