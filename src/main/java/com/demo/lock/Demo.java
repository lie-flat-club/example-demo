package com.demo.lock;

import java.util.HashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Demo {

  private static ReentrantLock lock = new ReentrantLock();
  private static Condition a = lock.newCondition();

  public static void main(String[] args) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println("xxx");
        try {
          a.await();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println("1111");
      }
    }).start();
    new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println("xxx");
        try {
          a.await();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }).start();
    new Thread(new Runnable() {
      @Override
      public void run() {
        System.out.println("xxx");
        a.signal();
      }
    }).start();

    //lock.readLock();
    // 写锁到等待读锁读完
    //lock.writeLock();
    // 读锁要等待写锁写完
    // 读锁不需要等待读锁读完
  }
}
