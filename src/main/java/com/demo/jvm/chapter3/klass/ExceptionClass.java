package com.demo.jvm.chapter3.klass;

public class ExceptionClass {

  public static void main(String[] args) {
    ExceptionClass exceptionClass = new ExceptionClass();
    System.out.println(exceptionClass.exec1());
  }

  private int exec1(){
    int i;
    try {
      i =  exec2();
      int a = 1/0;
      return i;
    } catch (NullPointerException e) {
      e.printStackTrace();
    } finally {
      return -1;
    }

  }
  private int exec2() throws Exception{
    return 2;
  }
}

/**
 * 0: aload_0       //  将第一个引用类型本地变量推送至栈顶。因为exec1是实例方法，则首个局部变量为this;
 * 1: invokespecial #2                  // Method exec2:()I    // 调用超类构建方法, 实例初始化方法, 私有方法;  这里调用的是exec2方法
 * 4: istore_1                  // 将栈顶 int 型数值存入第二个本地变量; 这里是i。
 * 5: iload_1                    // 将第二个 int 型本地变量推送至操作数栈栈顶；   i =  exec2()
 * 6: istore_2                    // 将栈顶 int 型数值存入第三个本地变量  ----这里是返回值  return i
 * 7: iconst_m1                 // finally 代码块：将 int 型-1 推送至栈顶；
 * 8: ireturn                   // finally 代码块：return -1;
 * 9: astore_2                  // 这里是catch代码： 将栈顶引用型数值存入第三个本地变量
 * 10: aload_2                  // 这里是catch代码：将第三个引用类型本地变量推送至栈顶   ;  Exception e
 * 11: invokevirtual #4          // Method java/lang/Exception.printStackTrace:()V
 * 14: iconst_m1                // 这里是开始finally块代码：  将 int 型-1 推送至栈顶；
 * 15: ireturn                   //  这里是开始finally块代码：  return -1;
 * 16: astore_3                  // 保存结果
 * 17: iconst_m1                // （真正的finally）这里是开始finally块代码：  将 int 型-1 推送至栈顶；
 * 18: ireturn                 // （真正的finally）这里是开始finally块代码：  return -1;
 */
/**
Exception table:
    from    to  target type
    0     7     9   Class java/lang/Exception
    0     7    16   any
    9    14    16   any
    16    18    16   any
*/
