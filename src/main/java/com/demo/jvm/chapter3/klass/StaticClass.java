package com.demo.jvm.chapter3.klass;

public class StaticClass {

  public StaticClass() {
    System.out.println("-----");
  }

  public static Integer a = null;

  static {
    a = new Integer(234);
    a = new Integer(123);
  }

  public static void main(String[] args) {
    System.out.println(a);
  }
}