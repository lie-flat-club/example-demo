package com.demo.jvm.chapter3.klass;

/**
 * 非主动使用类字段演示
 **/
public class NotInitialization {

  public static void main(String[] args) {
    System.out.println(SubClass.a);

  }
}

class OrderDetail {

  public static int value;

  static {
    System.out.println("SuperClass init!");
  }
}

class SubClass extends OrderDetail {

  static {
    System.out.println("SubClass init!");
  }

  public final static int a = 123;
}

class ConstClass {

  static {
    System.out.println("ConstClass init!");
  }

  public static final String HELLOWORLD = "hello world";
}