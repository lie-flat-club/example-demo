package com.demo.jvm.chapter2.gc;

import java.lang.management.ManagementFactory;

public class EdenTest {

  private static final int _1MB = 1024 * 1024;

  /**
   * VM参数：
   * -verbose:gc -Xms20M -Xmx20M -Xmn10M
   * eden 8M
   * s0 1M
   * s1 1M
   * old 10M
   * -XX:SurvivorRatio=8 -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -Xloggc:/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/java/com/demo/jvm/chapter3/edenTest-gc.log
   * jstat -gcutil 34846 2000
   */
  public static void main(String[] args) throws InterruptedException {
    byte[] allocation1, allocation2, allocation3, allocation4;
    String name = ManagementFactory.getRuntimeMXBean().getName();
    System.out.println(name);
    Thread.sleep(10000);
    allocation1 = new byte[2 * _1MB];
    System.out.println("eden区2M/8M");
    Thread.sleep(10000);
    allocation2 = new byte[2 * _1MB];
    System.out.println("eden区4M/8M");
    Thread.sleep(10000);
    allocation3 = new byte[2 * _1MB];
    System.out.println("eden区2M/8M, S区, old区4M/10M");
    Thread.sleep(10000);
    allocation4 = new byte[4 * _1MB];  // 出现一次Minor GC
    System.out.println("eden区6M/8M, S区，old区4M/10M");
    Thread.sleep(10000);
  }
}
