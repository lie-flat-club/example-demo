package com.demo.jvm.chapter2.gc;

public class BigObject2OldTest {

  private static final int _1MB = 1024 * 1024;

  /**
   * VM参数：-verbose:gc
   * -Xms20M
   * -Xmx20M
   * -Xmn10M
   * -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC
   * -XX:PretenureSizeThreshold=3145728
   */
  public static void main(String[] args) throws InterruptedException {
    //直接分配在老年代中
    Thread.sleep(10000);
    byte[] allocation;
    allocation = new byte[4 * _1MB];
    Thread.sleep(10000);
  }
}
