package com.demo.jvm.chapter2.gc;

public class DyncAgetoOld {

  private static final int _1MB = 1024 * 1024;

  /**
   * VM参数：-verbose:gc -Xms20M -Xmx20M -Xmn10M
   * -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:+PrintTenuringDistribution
   */
  public static void main(String[] args) throws InterruptedException {
    Thread.sleep(10000);
    byte[] allocation1, allocation2, allocation3, allocation4, allocation5, allocation6;
    allocation1 = new byte[_1MB / 4];  // allocation1+allocation2大于survivo空间一半
    allocation2 = new byte[_1MB / 4];
    allocation3 = new byte[3 * _1MB];
    allocation4 = new byte[3 * _1MB];
    Thread.sleep(10000);
    allocation5 = new byte[3 * _1MB];
    allocation6 = new byte[3 * _1MB];
    Thread.sleep(10000);
  }
}
