package com.demo.jvm.chapter2.heap;

public class HashCodeTest {

  /**
   * vm option:
   * -XX:hashCode=3
   * -XX:Xms256M
   */
  public static void main(String[] args) throws Exception {

    Object ob = new Object();
    Object ob2 = new Object();
    Object ob3 = new Object();
    Object ob4 = new Object();
    System.out.println(ob.hashCode());
    System.out.println(ob.hashCode());
    System.out.println(ob2.hashCode());
    System.out.println(ob3.hashCode());
    System.out.println(ob4.hashCode());

  }
}