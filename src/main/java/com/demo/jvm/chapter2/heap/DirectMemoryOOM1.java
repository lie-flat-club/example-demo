package com.demo.jvm.chapter2.heap;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class DirectMemoryOOM1 {

  /**
   *
   * vm option:
   * -Xmx20M -XX:MaxDirectMemorySize=10M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/java/com/demo/jvm
   */
  public static void main(String[] args) throws Exception {
    final int _1M = 1024 * 1024;
    List<ByteBuffer> buffers = new ArrayList<>();
    int count = 1;
    while (true) {
      ByteBuffer byteBuffer = ByteBuffer.allocateDirect(_1M);
      buffers.add(byteBuffer);
      if (count == 9) {
        while (true) {
          Thread.sleep(1000);
          System.out.println("睡眠中");
        }
      }
      System.out.println(count++);
    }
  }
}