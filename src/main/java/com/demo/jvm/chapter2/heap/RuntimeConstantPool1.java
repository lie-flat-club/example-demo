package com.demo.jvm.chapter2.heap;

public class RuntimeConstantPool1 {

  public static void main(String[] args) {
    one();
    System.out.println("========");
    //two();
  }

  public static void one() {
    // 创建了两个对象
    //String s = new String("1");

    String temp = "1";
    String s = new String(temp);

    s.intern();
    // 只创建了一个对象，只在常量池里面
    String s2 = "1";
    //System.out.println(s == s2);


    String s3 = new String("1") + new String("1");

    String s5 = s3.intern();

    // String s4 = "11";
    // String a = "11"; 去常量池里面找对应的字符串
    //String s4 = "11";
    // 1. jdk8 判断常量池或堆中是否有11。 2. 如果有，直接返回常量池中11的地址。 3. 如果没有就在常量池中新建11，并返回常量池中11的地址。

    //System.out.println(s3 == s4);
    System.out.println(s3 == s5);


    //System.out.println(s3 == s3.intern());
  }

  public static void two() {
    String s = new String("1");
    String s2 = "1";
    s.intern();
    System.out.println(s == s2);

    String s3 = new String("1") + new String("1");
    String s4 = "11";
    s3.intern();
    System.out.println(s3 == s4);
    //System.out.println(s3 == s3.intern());
  }
}