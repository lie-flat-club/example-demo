package com.demo.jvm.chapter2.heap;

import java.lang.reflect.Field;
import sun.misc.Unsafe;

public class DirectMemoryOOM2 {

  private static final int _1MB = 1024 * 1024;

  /**
   * todo 挂不了
   * vm option:
   * -Xmx20M -XX:MaxDirectMemorySize=10M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/java/com/demo/jvm
   */
  public static void main(String[] args) throws Exception {
    Field unsafeField = Unsafe.class.getDeclaredField("theUnsafe");
    unsafeField.setAccessible(true);
    Unsafe unsafe = (Unsafe) unsafeField.get(null);
    while (true) {
      unsafe.allocateMemory(_1MB);
    }
  }
}