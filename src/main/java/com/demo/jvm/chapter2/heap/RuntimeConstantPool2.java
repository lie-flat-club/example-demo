package com.demo.jvm.chapter2.heap;

import java.util.Random;

public class RuntimeConstantPool2 {

  final String a = "123123";

  static final int MAX = 1000 * 10000;
  static final String[] arr = new String[MAX];

  public static void main(String[] args) throws Exception {

    String b = "123123123";


    Integer[] DB_DATA = new Integer[10];
    Random random = new Random(10 * 10000);
    for (int i = 0; i < DB_DATA.length; i++) {
      DB_DATA[i] = random.nextInt();
    }
    long t = System.currentTimeMillis();
    for (int i = 0; i < MAX; i++) {
      // 1000个对象，其中500个对象是 "1"，另外500个对象是"11"
      arr[i] = new String(String.valueOf(DB_DATA[i % DB_DATA.length]));
      // 常量池会有2 个对象，一个对象是"a1"，另一个对象是"a11"
      arr[i] = ("a" + DB_DATA[i % DB_DATA.length]).intern();
    }

    System.out.println((System.currentTimeMillis() - t) + "ms");
    System.gc();
  }
}