package com.demo.jvm.chapter2.heap;

public class JavaVMStackSOF1 {

  private int stackLength = 1;

  public void stackLeak() {
    stackLength++;
    stackLeak();
  }

  /**
   * vm option:
   * -Xss512k
   * -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/java/com/demo/jvm
   */
  public static void main(String[] args) throws Throwable {
    JavaVMStackSOF1 oom = new JavaVMStackSOF1();
    try {
      // 6354 会异常
      oom.stackLeak();
    } catch (Throwable e) {
      System.out.println("stack length:" + oom.stackLength);
      throw e;
    }
  }
}