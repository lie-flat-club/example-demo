package com.demo.lucene;

import java.io.IOException;
import java.nio.file.Paths;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class LuceneFile {

  public static void main(String[] args) throws IOException, ParseException {
    addDocument();
  }

  private static void addDocument() throws IOException {
    IndexWriter writer = null;
    // todo 指定索引地址，使用 luke 可视化观察
    Directory directory = FSDirectory.open(Paths.get("/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/resources/test"));
    IndexWriterConfig config = new IndexWriterConfig(new WhitespaceAnalyzer());
    // 默认将 doc pos tim tip 合并至 cfs/cfe。这里设置为 false，将其单独成文件，方便学习。es也设置的false
    config.setUseCompoundFile(false);
    writer = new IndexWriter(directory, config);
    System.out.println(writer.getConfig());
    // 选择一段文本
    Document doc = new Document();
    doc.add(new Field("a", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("b", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("c", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("d", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("e", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("f", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("g", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("f", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("i", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("j", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("k", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("z", "this is first fields text", TextField.TYPE_STORED));
    doc.add(new Field("a", "this is first fields text", TextField.TYPE_STORED));
    writer.addDocument(doc);
    writer.commit();
    writer.close();
  }

  private static void search() throws IOException, ParseException {
    IndexSearcher searcher = new IndexSearcher(DirectoryReader
        .open(FSDirectory.open(Paths.get("/Users/lwg/SouceCode/private/lie-flat-club/example-demo/src/main/resources/test"))));
    QueryParser parser = new QueryParser("firstField1", new StandardAnalyzer());

    Query query = parser.parse("first");
    TopDocs topDocs = searcher.search(query, 10);
    for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
      Document document = searcher.doc(scoreDoc.doc);
      System.out.println("搜索" + document);
    }
    // Document document = searcher.doc(1);
  }
}
